use std::{
    io::{stdout, Error},
    sync::mpsc::{self, Receiver, Sender},
};
use termion::event::Key;

use termion::raw::IntoRawMode;

mod input;
mod renderer;

fn main() -> Result<(), Error> {
    let (input_sender, input_receiver): (Sender<Key>, Receiver<Key>) = mpsc::channel();

    let mut stdout = stdout()
        .into_raw_mode()
        .expect("Unable to capture stdout. Exiting.");

    input::capture(input_sender);
    renderer::start(&mut stdout, input_receiver);

    Ok(())
}
