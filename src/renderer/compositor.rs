use super::AlmostDrawable;
use super::Drawable;
use super::PixelColor;
use termion::event::Key;

pub fn get_pixels(
  frame: u128,
  keypress: Option<Key>,
  active_color: &PixelColor,
) -> (Vec<Drawable>, PixelColor) {
  let x_offset = 23;
  let y_offset = 8;
  let true_active_color = match keypress {
    Some(press) => get_active_color(press, &active_color),
    None => *active_color,
  };

  (
    vec![
      HEART_PROTOTYPE
        .lines()
        .collect::<Vec<&str>>()
        .iter()
        .rev()
        .enumerate()
        .map(move |(y_idx, line)| {
          let result = line.chars().enumerate().map(move |(x_idx, single_char)| {
            let almost = AlmostDrawable {
              x: x_idx as u16 + 1 + x_offset,
              y: y_idx as u16 + y_offset,
              character: single_char,
            };
            return almost;
          });
          return result;
        })
        .flatten()
        .enumerate()
        .map(|(idx, almost)| Drawable {
          x: almost.x,
          y: almost.y,
          character: almost.character,
          pixel_color: flipperoni(idx % 3 == 0),
        })
        .map(|drawable| Drawable {
          pixel_color: swap_if_necessary(drawable.pixel_color, frame % 2 == 0, &true_active_color),
          ..drawable
        })
        .collect::<Vec<Drawable>>(),
      valentine_message(frame),
      exit_message(frame),
    ]
    .into_iter()
    .flatten()
    .collect(),
    true_active_color,
  )
}

fn get_active_color(keypress: Key, active_color: &PixelColor) -> PixelColor {
  match keypress {
    Key::Char('a') | Key::Char('A') => PixelColor {
      // Abel Tasman
      r: 103,
      g: 89,
      b: 46,
    },
    Key::Char('b') | Key::Char('B') => PixelColor {
      // Blue
      r: 0,
      g: 112,
      b: 255,
    },
    Key::Char('c') | Key::Char('C') => PixelColor {
      // Cadmium scarlet
      r: 206,
      g: 59,
      b: 49,
    },
    Key::Char('d') | Key::Char('D') => PixelColor {
      // Daisy
      r: 241,
      g: 219,
      b: 45,
    },
    Key::Char('e') | Key::Char('E') => PixelColor {
      // Eggplant
      r: 153,
      g: 0,
      b: 102,
    },
    Key::Char('f') | Key::Char('F') => PixelColor {
      // Facebook navy blue
      r: 38,
      g: 72,
      b: 172,
    },
    Key::Char('g') | Key::Char('G') => PixelColor {
      // GO green
      r: 0,
      g: 171,
      b: 102,
    },
    Key::Char('h') | Key::Char('H') => PixelColor {
      // Halaya Ube
      r: 102,
      g: 56,
      b: 84,
    },
    Key::Char('i') | Key::Char('I') => PixelColor {
      // Indigo
      r: 111,
      g: 0,
      b: 255,
    },
    Key::Char('j') | Key::Char('J') => PixelColor {
      // Jack-O-Lantern Orange
      r: 244,
      g: 120,
      b: 30,
    },
    Key::Char('k') | Key::Char('K') => PixelColor {
      // Kelly green
      r: 76,
      g: 187,
      b: 23,
    },
    Key::Char('l') | Key::Char('L') => PixelColor {
      // Lavender
      r: 181,
      g: 126,
      b: 220,
    },
    Key::Char('m') | Key::Char('M') => PixelColor {
      // Maroon
      r: 128,
      g: 0,
      b: 0,
    },
    Key::Char('n') | Key::Char('N') => PixelColor {
      // Neon Chartreuse
      r: 213,
      g: 207,
      b: 49,
    },
    Key::Char('o') | Key::Char('O') => PixelColor {
      // Oak brown
      r: 180,
      g: 104,
      b: 52,
    },
    Key::Char('p') | Key::Char('P') => PINK,
    Key::Char('q') | Key::Char('Q') => PixelColor {
      // Quick silver
      r: 166,
      g: 166,
      b: 166,
    },
    Key::Char('r') | Key::Char('R') => PixelColor {
      // Razzmatazz
      r: 227,
      g: 11,
      b: 92,
    },
    Key::Char('s') | Key::Char('S') => PixelColor {
      // siam
      r: 100,
      g: 106,
      b: 84,
    },
    Key::Char('t') | Key::Char('T') => PixelColor {
      // Turquoise
      r: 127,
      g: 255,
      b: 255,
    },
    Key::Char('u') | Key::Char('U') => PixelColor {
      // Ultra Pink
      r: 255,
      g: 111,
      b: 255,
    },
    Key::Char('v') | Key::Char('V') => PixelColor {
      // Violet
      r: 143,
      g: 0,
      b: 255,
    },
    Key::Char('w') | Key::Char('W') => PixelColor {
      // Wanderlust
      r: 136,
      g: 132,
      b: 60,
    },
    Key::Char('x') | Key::Char('X') => PixelColor {
      // Xanthic
      r: 238,
      g: 237,
      b: 9,
    },
    Key::Char('y') | Key::Char('Y') => PixelColor {
      // Yellow
      r: 255,
      g: 255,
      b: 0,
    },
    Key::Char('z') | Key::Char('Z') => PixelColor {
      // Zunza
      r: 119,
      g: 209,
      b: 188,
    },
    _ => *active_color,
  }
}

fn exit_message(frame: u128) -> Vec<Drawable> {
  if frame < 9 {
    return vec![];
  }

  EXIT_MESSAGE
    .chars()
    .enumerate()
    .map(|(idx, character)| Drawable {
      x: idx as u16 + 1,
      y: 1,
      character: character,
      pixel_color: GREY,
    })
    .collect()
}

fn valentine_message(frame: u128) -> Vec<Drawable> {
  if frame < 5 {
    return vec![];
  }

  BEAN_MESSAGE
    .chars()
    .enumerate()
    .map(|(idx, character)| Drawable {
      x: idx as u16 + 1,
      y: 0,
      character: character,
      pixel_color: WHITE,
    })
    .collect()
}

// this is so gross but who cares
fn swap_if_necessary(
  color: PixelColor,
  should_swap: bool,
  true_active_color: &PixelColor,
) -> PixelColor {
  match color {
    PINK => {
      if should_swap {
        WARM_WHITE
      } else {
        *true_active_color
      }
    }
    WARM_WHITE => {
      if should_swap {
        *true_active_color
      } else {
        WARM_WHITE
      }
    }
    _ => WARM_WHITE,
  }
}

fn flipperoni(yeet: bool) -> PixelColor {
  if yeet {
    PINK
  } else {
    WARM_WHITE
  }
}

const BEAN_MESSAGE: &str = "Happy Valentine's day my tub ;D I kinda love yous a lot";
const EXIT_MESSAGE: &str = "^C to quit (or press any other letter to change color)";

const HEART_PROTOTYPE: &str = r#"
     _____           _____
  ,ad8PPPP88b,     ,d88PPPP8ba,
 d8P"      "Y8b, ,d8P"      "Y8b
dP'           "8a8"           `Yb
8(              "              )8
I8                             8I
 Yb,                         ,dP
  "8a,                     ,a8"
    "8a,                 ,a8"
      "Yba             adP"
        `Y8a         a8P'
          `88,     ,88'
            "8b   d8"
             "8b d8"
              `888'
                "
"#;

pub const PINK: PixelColor = PixelColor {
  r: 255,
  g: 108,
  b: 105,
};

pub const WARM_WHITE: PixelColor = PixelColor {
  r: 255,
  g: 227,
  b: 246,
};

pub const WHITE: PixelColor = PixelColor {
  r: 255,
  g: 255,
  b: 255,
};

pub const GREY: PixelColor = PixelColor {
  r: 192,
  g: 192,
  b: 192,
};
