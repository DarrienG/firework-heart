use std::{io::stdin, sync::mpsc::Sender, thread};
use termion::{event::*, input::TermRead};

pub fn capture(sender: Sender<Key>) {
    thread::spawn(|| start_input_capture(sender));
}

fn start_input_capture(sender: Sender<Key>) {
    for c in stdin().events() {
        match c.unwrap() {
            Event::Key(v) => {
                sender
                    .send(v)
                    .expect("Unable to send keypress, something funky has happened.");
            }
            _ => (),
        }
    }
}
