use std::{
    io::{Error, Stdout, Write},
    sync::mpsc::Receiver,
    time::Duration,
};
use termion::{color::Rgb, cursor::Goto, event::Key};

mod compositor;

pub struct AlmostDrawable {
    pub x: u16,
    pub y: u16,
    pub character: char,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Drawable {
    pub x: u16,
    pub y: u16,
    pub character: char,
    pub pixel_color: PixelColor,
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct PixelColor {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

pub fn start(stdout: &mut Stdout, user_keypresses: Receiver<Key>) {
    write!(stdout, "{}{}", termion::clear::All, termion::cursor::Hide)
        .expect("Unable to clear terminal for writing");
    let mut counter: u128 = 0;
    let mut active_color = compositor::PINK;
    loop {
        if let Ok(v) = user_keypresses.recv_timeout(Duration::from_millis(400)) {
            match v {
                Key::Ctrl('c') => {
                    write!(stdout, "{}{}", termion::clear::All, termion::cursor::Show)
                        .expect("Unable to restore terminal");
                    return;
                }
                all_else => {
                    let (drawables, new_color) =
                        compositor::get_pixels(counter, Some(all_else), &active_color);
                    active_color = new_color;
                    output_grid(drawables, stdout).expect("Unable to write to terminal. Exiting.");
                }
            }
        } else {
            let (drawables, new_color) = compositor::get_pixels(counter, None, &active_color);
            active_color = new_color;
            output_grid(drawables, stdout).expect("Unable to write to terminal. Exiting.");
        }

        counter += 1;
    }
}

fn output_grid(drawables: Vec<Drawable>, stdout: &mut Stdout) -> Result<(), Error> {
    let (length, width) = terminal_length_width();
    for point in drawables.iter() {
        if point.y < width {
            if point.character != ' ' {
                write!(
                    stdout,
                    "{}{}{}{}",
                    Goto(point.x % length, width - point.y),
                    termion::cursor::Hide,
                    Rgb(
                        point.pixel_color.r,
                        point.pixel_color.g,
                        point.pixel_color.b,
                    )
                    .fg_string(),
                    point.character,
                )?;
            }
        }
    }
    stdout.flush()?;
    Ok(())
}

fn terminal_length_width() -> (u16, u16) {
    termion::terminal_size().expect("Unable to get terminal size")
}
