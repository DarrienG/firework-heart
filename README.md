# firework-heart

A fun little terminal heart for Valentine's day.

Strongly recommended that your terminal app support truecolor, otherwise things
may look a little weird.

Your default terminal app on Linux probably does, on macOS it does not. You
should download iterm 2.

See that bad larry in action [here](https://asciinema.org/a/468555)
